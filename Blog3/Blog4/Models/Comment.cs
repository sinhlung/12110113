﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [StringLength(1000, ErrorMessage = "so ky tu tối thiểu 50", MinimumLength = 50)]
        public String Body { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        public DateTime DateCreated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        [Required(ErrorMessage = "ko được để trống")]
        public string Author { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}
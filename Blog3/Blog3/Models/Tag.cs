﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_3.Models
{
    public class Tag
    {
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int TagID { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [StringLength(100, ErrorMessage = "so ky tu trong khoang 10-100", MinimumLength = 10)]
        public string Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Blog_3.Models
{
    public class BlogDbContext : DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasMany(d => d.Tags).WithMany(p => p.Posts).Map(t => t.MapLeftKey("PostID")
                .MapRightKey("tagID").ToTable("Tag_Post"));
        }
        
        public DbSet<Tag> Tags { set; get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_3.Models
{
    [Table("BaiViet")]
    public class Post
    {
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [StringLength(500, ErrorMessage = "so ky tu trong khoang 20-500", MinimumLength = 20)]
        public string Title { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [StringLength(1000, ErrorMessage = "so ky tu tối thiểu 50", MinimumLength = 50)]
        public string Body { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [DataType(DataType.DateTime, ErrorMessage = "dữ liệu nhập vào phải là mm/dd/yy")]
        public DateTime DateCreated { set; get; }  

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

    }
}
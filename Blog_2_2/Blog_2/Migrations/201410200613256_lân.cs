namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lân : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts");
            DropIndex("dbo.BaiViet", new[] { "AccountID" });
            AddColumn("dbo.Accounts", "ID", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.Accounts", new[] { "AccountID" });
            AddPrimaryKey("dbo.Accounts", "ID");
            AddForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts", "ID", cascadeDelete: true);
            CreateIndex("dbo.BaiViet", "AccountID");
            DropColumn("dbo.Accounts", "AccountID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Accounts", "AccountID", c => c.Int(nullable: false));
            DropIndex("dbo.BaiViet", new[] { "AccountID" });
            DropForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts");
            DropPrimaryKey("dbo.Accounts", new[] { "ID" });
            AddPrimaryKey("dbo.Accounts", "AccountID");
            DropColumn("dbo.Accounts", "ID");
            CreateIndex("dbo.BaiViet", "AccountID");
            AddForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace Blog.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        public string Title { set; get; }
        public string body { get; set; }
    }
    public class BlogDBContext : DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }
    }
}
namespace ViecLam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class L7 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DangViecLams", "ViPham_ID", "dbo.ViPhams");
            DropForeignKey("dbo.ChiaSeKinhNghiems", "ViPham_ID", "dbo.ViPhams");
            DropIndex("dbo.DangViecLams", new[] { "ViPham_ID" });
            DropIndex("dbo.ChiaSeKinhNghiems", new[] { "ViPham_ID" });
            AddColumn("dbo.DangViecLams", "LoiViPham", c => c.String());
            AddColumn("dbo.ChiaSeKinhNghiems", "LoiViPham", c => c.String());
            DropColumn("dbo.DangViecLams", "ViPhamID");
            DropColumn("dbo.DangViecLams", "ViPham_ID");
            DropColumn("dbo.ChiaSeKinhNghiems", "ViPham_ID");
            DropTable("dbo.ViPhams");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ViPhams",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LoiViPham = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.ChiaSeKinhNghiems", "ViPham_ID", c => c.Int());
            AddColumn("dbo.DangViecLams", "ViPham_ID", c => c.Int());
            AddColumn("dbo.DangViecLams", "ViPhamID", c => c.String());
            DropColumn("dbo.ChiaSeKinhNghiems", "LoiViPham");
            DropColumn("dbo.DangViecLams", "LoiViPham");
            CreateIndex("dbo.ChiaSeKinhNghiems", "ViPham_ID");
            CreateIndex("dbo.DangViecLams", "ViPham_ID");
            AddForeignKey("dbo.ChiaSeKinhNghiems", "ViPham_ID", "dbo.ViPhams", "ID");
            AddForeignKey("dbo.DangViecLams", "ViPham_ID", "dbo.ViPhams", "ID");
        }
    }
}

// <auto-generated />
namespace ViecLam.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class L12 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(L12));
        
        string IMigrationMetadata.Id
        {
            get { return "201412140545293_L12"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

namespace ViecLam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class L4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DangViecLams", "UserProfile_UserId", "dbo.UserProfile");
            DropIndex("dbo.DangViecLams", new[] { "UserProfile_UserId" });
            RenameColumn(table: "dbo.DangViecLams", name: "UserProfile_UserId", newName: "UserProfileUserID");
            AddForeignKey("dbo.DangViecLams", "UserProfileUserID", "dbo.UserProfile", "UserId", cascadeDelete: true);
            CreateIndex("dbo.DangViecLams", "UserProfileUserID");
            DropColumn("dbo.DangViecLams", "UserProfileID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DangViecLams", "UserProfileID", c => c.Int(nullable: false));
            DropIndex("dbo.DangViecLams", new[] { "UserProfileUserID" });
            DropForeignKey("dbo.DangViecLams", "UserProfileUserID", "dbo.UserProfile");
            RenameColumn(table: "dbo.DangViecLams", name: "UserProfileUserID", newName: "UserProfile_UserId");
            CreateIndex("dbo.DangViecLams", "UserProfile_UserId");
            AddForeignKey("dbo.DangViecLams", "UserProfile_UserId", "dbo.UserProfile", "UserId");
        }
    }
}

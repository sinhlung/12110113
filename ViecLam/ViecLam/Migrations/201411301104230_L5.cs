namespace ViecLam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class L5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserProfile", "PhanThuongID", "dbo.PhanThuongs");
            DropIndex("dbo.UserProfile", new[] { "PhanThuongID" });
            AddColumn("dbo.ChiaSeKinhNghiems", "PhanThuongID", c => c.Int(nullable: false));
            AddForeignKey("dbo.ChiaSeKinhNghiems", "PhanThuongID", "dbo.PhanThuongs", "ID", cascadeDelete: true);
            CreateIndex("dbo.ChiaSeKinhNghiems", "PhanThuongID");
            DropColumn("dbo.UserProfile", "PhanThuongID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfile", "PhanThuongID", c => c.Int(nullable: false));
            DropIndex("dbo.ChiaSeKinhNghiems", new[] { "PhanThuongID" });
            DropForeignKey("dbo.ChiaSeKinhNghiems", "PhanThuongID", "dbo.PhanThuongs");
            DropColumn("dbo.ChiaSeKinhNghiems", "PhanThuongID");
            CreateIndex("dbo.UserProfile", "PhanThuongID");
            AddForeignKey("dbo.UserProfile", "PhanThuongID", "dbo.PhanThuongs", "ID", cascadeDelete: true);
        }
    }
}

namespace ViecLam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class L8 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DangViecLams", "NgayDang", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ChiaSeKinhNghiems", "NgayDang", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ChiaSeKinhNghiems", "NgayDang", c => c.String());
            DropColumn("dbo.DangViecLams", "NgayDang");
        }
    }
}

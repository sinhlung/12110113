namespace ViecLam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class L10 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DangViecLams", "MoTa", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DangViecLams", "MoTa", c => c.String());
        }
    }
}

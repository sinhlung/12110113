namespace ViecLam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class L12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChiaSeKinhNghiems", "TaiKhoan", c => c.String());
            DropColumn("dbo.Comments", "TaiKhoan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "TaiKhoan", c => c.String());
            DropColumn("dbo.ChiaSeKinhNghiems", "TaiKhoan");
        }
    }
}

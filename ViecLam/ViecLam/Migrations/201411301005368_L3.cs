namespace ViecLam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class L3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ViecLams", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.ViecLams", "TheLoaiID", "dbo.TheLoais");
            DropForeignKey("dbo.ViecLams", "NganhID", "dbo.Nganhs");
            DropForeignKey("dbo.ViecLams", "ViPham_ID", "dbo.ViPhams");
            DropIndex("dbo.ViecLams", new[] { "UserProfile_UserId" });
            DropIndex("dbo.ViecLams", new[] { "TheLoaiID" });
            DropIndex("dbo.ViecLams", new[] { "NganhID" });
            DropIndex("dbo.ViecLams", new[] { "ViPham_ID" });
            CreateTable(
                "dbo.DangViecLams",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenViecLam = c.String(),
                        NoiDung = c.String(),
                        UserProfileID = c.Int(nullable: false),
                        TheLoaiID = c.Int(nullable: false),
                        NganhID = c.Int(nullable: false),
                        ViPhamID = c.String(),
                        UserProfile_UserId = c.Int(),
                        ViPham_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.TheLoais", t => t.TheLoaiID, cascadeDelete: true)
                .ForeignKey("dbo.Nganhs", t => t.NganhID, cascadeDelete: true)
                .ForeignKey("dbo.ViPhams", t => t.ViPham_ID)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.TheLoaiID)
                .Index(t => t.NganhID)
                .Index(t => t.ViPham_ID);
            
            DropTable("dbo.ViecLams");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ViecLams",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenViecLam = c.String(),
                        NoiDung = c.String(),
                        UserProfileID = c.Int(nullable: false),
                        TheLoaiID = c.Int(nullable: false),
                        NganhID = c.Int(nullable: false),
                        ViPhamID = c.String(),
                        UserProfile_UserId = c.Int(),
                        ViPham_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            DropIndex("dbo.DangViecLams", new[] { "ViPham_ID" });
            DropIndex("dbo.DangViecLams", new[] { "NganhID" });
            DropIndex("dbo.DangViecLams", new[] { "TheLoaiID" });
            DropIndex("dbo.DangViecLams", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.DangViecLams", "ViPham_ID", "dbo.ViPhams");
            DropForeignKey("dbo.DangViecLams", "NganhID", "dbo.Nganhs");
            DropForeignKey("dbo.DangViecLams", "TheLoaiID", "dbo.TheLoais");
            DropForeignKey("dbo.DangViecLams", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.DangViecLams");
            CreateIndex("dbo.ViecLams", "ViPham_ID");
            CreateIndex("dbo.ViecLams", "NganhID");
            CreateIndex("dbo.ViecLams", "TheLoaiID");
            CreateIndex("dbo.ViecLams", "UserProfile_UserId");
            AddForeignKey("dbo.ViecLams", "ViPham_ID", "dbo.ViPhams", "ID");
            AddForeignKey("dbo.ViecLams", "NganhID", "dbo.Nganhs", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ViecLams", "TheLoaiID", "dbo.TheLoais", "ID", cascadeDelete: true);
            AddForeignKey("dbo.ViecLams", "UserProfile_UserId", "dbo.UserProfile", "UserId");
        }
    }
}

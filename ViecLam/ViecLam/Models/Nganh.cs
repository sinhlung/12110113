﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViecLam.Models
{
    public class Nganh
    {
      
        public int ID { set; get; }
        public string TenNganh { set; get; }

        public virtual ICollection<DangViecLam> DangViecLams { set; get; }
    }
}
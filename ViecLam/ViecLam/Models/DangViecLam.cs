﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViecLam.Models
{
    public class DangViecLam
    {
        public int ID { set; get; }
        public string TenViecLam { set; get; }
        public string NoiDung { set; get; }
        public DateTime NgayDang { set; get; }
        public string LoiViPham { set; get; }

        public int UserProfileID { set; get; }
        public virtual UserProfile UserProfile { set; get; }

        public int TheLoaiID { set; get; }
        public virtual TheLoai TheLoai { set; get; }
        public int NganhID { set; get; }
        public virtual Nganh Nganh { set; get; }
        
    }
}
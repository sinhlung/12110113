﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViecLam.Models
{
    public class PhanThuong
    {
        public int ID { set; get; }
        public long SoBaiViet { set; get; }
        public String PhanQua { set; get; }

        public virtual ICollection<ChiaSeKinhNghiem> ChiaSeKinhNghiems { set; get; }
    }
}
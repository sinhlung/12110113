﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViecLam.Models
{
    public class ChiaSeKinhNghiem
    {
        public int ID { set; get; }
        public string ChuDe { set; get; }
        public string NoiDung { set; get; }
        public DateTime NgayDang { set; get; }
        public string LoiViPham { set; get; }

        public int UserID  { set; get; }
        public virtual UserProfile UserProfile { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public int PhanThuongID { get; set; }
        public virtual PhanThuong PhanThuong { get; set; }
        
    }
}
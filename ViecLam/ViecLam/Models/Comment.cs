﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViecLam.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public string TaiKhoan { set; get; }
        public string NoiDung { set; get; }
        public DateTime NgayDang { set; get; }       

        public int ChiaSeKinhNghiemID { set; get; }
        public virtual ChiaSeKinhNghiem ChiaSeKinhNghiem { set; get; }
    }
}
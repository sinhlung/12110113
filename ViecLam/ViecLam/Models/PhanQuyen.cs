﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ViecLam.Models
{
    public class PhanQuyen
    {
        [Key]
        public int MaPQ { set; get; }
        public string TenPQ { set; get; }

        public virtual ICollection<UserProfile> UserProfiles { set; get; }
    }
}
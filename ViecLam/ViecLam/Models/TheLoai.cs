﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViecLam.Models
{
    public class TheLoai
    {
        public int ID { set; get; }
        public string TenTL { set; get; }

        public virtual ICollection<DangViecLam> DangViecLams { set; get; }
    }
}
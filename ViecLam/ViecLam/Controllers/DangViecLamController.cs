﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViecLam.Models;

namespace ViecLam.Controllers
{
    [Authorize]
    public class DangViecLamController : Controller
    {
        private ViecLamDbContext db = new ViecLamDbContext();

        //
        // GET: /DangViecLam/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var dangvieclams = db.DangViecLams.Include(d => d.UserProfile);
            return View(dangvieclams.ToList());
        }

        //
        // GET: /DangViecLam/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            DangViecLam dangvieclam = db.DangViecLams.Find(id);
            if (dangvieclam == null)
            {
                return HttpNotFound();
            }
            return View(dangvieclam);
        }


       // Hiện Thị Thông Tin Khi Đang Ký Thành Công
        public ActionResult HienThi(int id = 0)
        {
            DangViecLam dangvieclam = db.DangViecLams.Find(id);
            if (dangvieclam == null)
            {
                return HttpNotFound();
            }
            return View(dangvieclam);

        }    

     

        // GET: /DangViecLam/Create

        public ActionResult Create()
        {
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTL");
            ViewBag.NganhID = new SelectList(db.Nganhs, "ID", "TenNganh");
            return View();
        }

        //
        // POST: /DangViecLam/Create

        [HttpPost]
        public ActionResult Create(DangViecLam dangvieclam)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                dangvieclam.UserProfileID = userid;
                dangvieclam.NgayDang = DateTime.Now;
                db.DangViecLams.Add(dangvieclam);
                db.SaveChanges();
               
                return RedirectToAction("HienThi/"+dangvieclam.ID,"DangViecLam");
               // return RedirectToAction("Index");
            }

            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTL", dangvieclam.TheLoaiID);
            ViewBag.NganhID = new SelectList(db.Nganhs, "ID", "TenNganh", dangvieclam.NganhID);
            return View(dangvieclam);
        }


        //
        // GET: /DangViecLam/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DangViecLam dangvieclam = db.DangViecLams.Find(id);
            if (dangvieclam == null)
            {
                return HttpNotFound();
            }
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTL", dangvieclam.TheLoaiID);
            ViewBag.NganhID = new SelectList(db.Nganhs, "ID", "TenNganh", dangvieclam.NganhID);
            return View(dangvieclam);
        }

        //
        // POST: /DangViecLam/Edit/5

        [HttpPost]
        public ActionResult Edit(DangViecLam dangvieclam)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dangvieclam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTL", dangvieclam.TheLoaiID);
            ViewBag.NganhID = new SelectList(db.Nganhs, "ID", "TenNganh", dangvieclam.NganhID);
            return View(dangvieclam);
        }

        //
        // GET: /DangViecLam/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DangViecLam dangvieclam = db.DangViecLams.Find(id);
            if (dangvieclam == null)
            {
                return HttpNotFound();
            }
            return View(dangvieclam);
        }

        //
        // POST: /DangViecLam/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            DangViecLam dangvieclam = db.DangViecLams.Find(id);
            db.DangViecLams.Remove(dangvieclam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
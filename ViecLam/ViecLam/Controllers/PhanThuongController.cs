﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViecLam.Models;

namespace ViecLam.Controllers
{
    public class PhanThuongController : Controller
    {
        private ViecLamDbContext db = new ViecLamDbContext();

        //
        // GET: /PhanThuong/

        public ActionResult Index()
        {
            return View(db.PhanThuongs.ToList());
        }

        //
        // GET: /PhanThuong/Details/5

        public ActionResult Details(int id = 0)
        {
            PhanThuong phanthuong = db.PhanThuongs.Find(id);
            if (phanthuong == null)
            {
                return HttpNotFound();
            }
            return View(phanthuong);
        }

        //
        // GET: /PhanThuong/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PhanThuong/Create

        [HttpPost]
        public ActionResult Create(PhanThuong phanthuong)
        {
            if (ModelState.IsValid)
            {
                db.PhanThuongs.Add(phanthuong);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(phanthuong);
        }

        //
        // GET: /PhanThuong/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PhanThuong phanthuong = db.PhanThuongs.Find(id);
            if (phanthuong == null)
            {
                return HttpNotFound();
            }
            return View(phanthuong);
        }

        //
        // POST: /PhanThuong/Edit/5

        [HttpPost]
        public ActionResult Edit(PhanThuong phanthuong)
        {
            if (ModelState.IsValid)
            {
                db.Entry(phanthuong).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(phanthuong);
        }

        //
        // GET: /PhanThuong/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PhanThuong phanthuong = db.PhanThuongs.Find(id);
            if (phanthuong == null)
            {
                return HttpNotFound();
            }
            return View(phanthuong);
        }

        //
        // POST: /PhanThuong/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PhanThuong phanthuong = db.PhanThuongs.Find(id);
            db.PhanThuongs.Remove(phanthuong);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViecLam.Models;

namespace ViecLam.Controllers
{
    public class NganhController : Controller
    {
        private ViecLamDbContext db = new ViecLamDbContext();

        //
        // GET: /Nganh/

        public ActionResult Index()
        {
            return View(db.Nganhs.ToList());
        }

        //
        // GET: /Nganh/Details/5

        public ActionResult Details(int id = 0)
        {
            Nganh nganh = db.Nganhs.Find(id);
            if (nganh == null)
            {
                return HttpNotFound();
            }
            return View(nganh);
        }

        //
        // GET: /Nganh/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Nganh/Create

        [HttpPost]
        public ActionResult Create(Nganh nganh)
        {
            if (ModelState.IsValid)
            {
                db.Nganhs.Add(nganh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nganh);
        }

        //
        // GET: /Nganh/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Nganh nganh = db.Nganhs.Find(id);
            if (nganh == null)
            {
                return HttpNotFound();
            }
            return View(nganh);
        }

        //
        // POST: /Nganh/Edit/5

        [HttpPost]
        public ActionResult Edit(Nganh nganh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nganh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nganh);
        }

        //
        // GET: /Nganh/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Nganh nganh = db.Nganhs.Find(id);
            if (nganh == null)
            {
                return HttpNotFound();
            }
            return View(nganh);
        }

        //
        // POST: /Nganh/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Nganh nganh = db.Nganhs.Find(id);
            db.Nganhs.Remove(nganh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
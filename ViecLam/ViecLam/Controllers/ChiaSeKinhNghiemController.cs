﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViecLam.Models;

namespace ViecLam.Controllers
{
    [Authorize]
    public class ChiaSeKinhNghiemController : Controller
    {
        private ViecLamDbContext db = new ViecLamDbContext();

        //
        // GET: /ChiaSeKinhNghiem/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var chiasekinhnghiems = db.ChiaSeKinhNghiems.Include(c => c.UserProfile);
            return View(chiasekinhnghiems.ToList());
        }

        //
        // GET: /ChiaSeKinhNghiem/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            ChiaSeKinhNghiem chiasekinhnghiem = db.ChiaSeKinhNghiems.Find(id);
            ViewData["idpost"] = id;
            if (chiasekinhnghiem == null)
            {
                return HttpNotFound();
            }
            return View(chiasekinhnghiem);
        }

        //
        // GET: /ChiaSeKinhNghiem/Create

        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /ChiaSeKinhNghiem/Create

        [HttpPost]
        public ActionResult Create(ChiaSeKinhNghiem chiasekinhnghiem)
        {
            if (ModelState.IsValid)
            {
                chiasekinhnghiem.PhanThuongID = 1;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                chiasekinhnghiem.UserID = userid;
                chiasekinhnghiem.NgayDang = DateTime.Now;
                db.ChiaSeKinhNghiems.Add(chiasekinhnghiem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.UserProfiles, "UserId", "UserName", chiasekinhnghiem.UserID);
            return View(chiasekinhnghiem);
        }

        //
        // GET: /ChiaSeKinhNghiem/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ChiaSeKinhNghiem chiasekinhnghiem = db.ChiaSeKinhNghiems.Find(id);
            if (chiasekinhnghiem == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.UserProfiles, "UserId", "UserName", chiasekinhnghiem.UserID);
            return View(chiasekinhnghiem);
        }

        //
        // POST: /ChiaSeKinhNghiem/Edit/5

        [HttpPost]
        public ActionResult Edit(ChiaSeKinhNghiem chiasekinhnghiem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chiasekinhnghiem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.UserProfiles, "UserId", "UserName", chiasekinhnghiem.UserID);
            return View(chiasekinhnghiem);
        }

        //
        // GET: /ChiaSeKinhNghiem/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ChiaSeKinhNghiem chiasekinhnghiem = db.ChiaSeKinhNghiems.Find(id);
            if (chiasekinhnghiem == null)
            {
                return HttpNotFound();
            }
            return View(chiasekinhnghiem);
        }

        //
        // POST: /ChiaSeKinhNghiem/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ChiaSeKinhNghiem chiasekinhnghiem = db.ChiaSeKinhNghiems.Find(id);
            db.ChiaSeKinhNghiems.Remove(chiasekinhnghiem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
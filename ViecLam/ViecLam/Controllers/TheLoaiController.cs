﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViecLam.Models;

namespace ViecLam.Controllers
{
    public class TheLoaiController : Controller
    {
        private ViecLamDbContext db = new ViecLamDbContext();

        //
        // GET: /TheLoai/

        public ActionResult Index()
        {
            return View(db.TheLoais.ToList());
        }

        //
        // GET: /TheLoai/Details/5

        public ActionResult Details(int id = 0)
        {
            TheLoai theloai = db.TheLoais.Find(id);
            if (theloai == null)
            {
                return HttpNotFound();
            }
            return View(theloai);
        }

        //
        // GET: /TheLoai/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TheLoai/Create

        [HttpPost]
        public ActionResult Create(TheLoai theloai)
        {
            if (ModelState.IsValid)
            {
                db.TheLoais.Add(theloai);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(theloai);
        }

        //
        // GET: /TheLoai/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TheLoai theloai = db.TheLoais.Find(id);
            if (theloai == null)
            {
                return HttpNotFound();
            }
            return View(theloai);
        }

        //
        // POST: /TheLoai/Edit/5

        [HttpPost]
        public ActionResult Edit(TheLoai theloai)
        {
            if (ModelState.IsValid)
            {
                db.Entry(theloai).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(theloai);
        }

        //
        // GET: /TheLoai/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TheLoai theloai = db.TheLoais.Find(id);
            if (theloai == null)
            {
                return HttpNotFound();
            }
            return View(theloai);
        }

        //
        // POST: /TheLoai/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TheLoai theloai = db.TheLoais.Find(id);
            db.TheLoais.Remove(theloai);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}